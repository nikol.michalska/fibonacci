package com.company;

public class Main {

    public static void main(String[] args) {

        int number = 10;
        for (int a = 0; a < number; a++) {
            System.out.println(getFib(a) + " ");
        }

    }

    public static int getFib(int number) {
        if (number <= 1) {
            return number;
        }
        return getFib(number - 1) + getFib(number - 2);
    }
}
